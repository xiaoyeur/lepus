module lepus

go 1.14

require (
	github.com/Shopify/sarama v1.28.0
	github.com/StackExchange/wmi v0.0.0-20210224194228-fe8f1750fd46 // indirect
	github.com/garyburd/redigo v1.6.2
	github.com/go-ole/go-ole v1.2.5 // indirect
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorhill/cronexpr v0.0.0-20180427100037-88b0669f7d75
	github.com/larspensjo/config v0.0.0-20160228172812-b6db95dc6321
	github.com/nsqio/go-nsq v1.1.0
	github.com/onsi/ginkgo v1.15.0 // indirect
	github.com/onsi/gomega v1.10.5 // indirect
	github.com/phachon/go-logger v0.0.0-20191215032019-86e4227f71ea
	github.com/satori/go.uuid v1.2.0
	github.com/shirou/gopsutil v3.21.2+incompatible
	github.com/tklauser/go-sysconf v0.3.4 // indirect
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
)
